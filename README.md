# Código fonte abeancos.gal Drupal 7

## Requirimentos

* PHP 7.4
* MySQL 5.7

## Restauración do sitio

1. Mover o directorio **drupal** ao directorio raíz do servidor web
2. Crear base de datos **MySQL**
3. Importar na base de datos creada no punto anterior o contido do ficheiro **abeancos.sql**
4. Configurar os datos de acceso á base de datos no ficheiro `drupal/sites/default/settings.php`

*NOTA: Neste repositorio non están incluídas as imaxes dos elementos catalogados no sitio web. Poden obterse dende o seguinte enderezo: https://www.abeancos.gal/datos_abertos*
 
### Ligazóns de interese

* https://api.drupal.org/api/drupal/sites%21default%21default.settings.php/7.x
* https://www.drupal.org/docs/7/install/step-3-create-settingsphp-and-the-files-directory
* https://www.drupal.org/docs/7/backing-up-and-migrating-a-site/restoring-a-database-backup-command-line

### Acceso:

As credenciais empregadas nesta instalación son as seguintes:

* Usuario: abeancos
* Contrasinal: abeancos

---

Os contidos deste repositorio están dispoñibles baixo unha licenza
[Creative Commons Attribution-ShareAlike 4.0 International License][cc-by-sa].

[![CC BY-SA 4.0][cc-by-sa-image]][cc-by-sa]

[cc-by-sa]: http://creativecommons.org/licenses/by-sa/4.0/
[cc-by-sa-image]: https://licensebuttons.net/l/by-sa/4.0/88x31.png

