<?php
/**
 * @file
 * abeancos.features.language.inc
 */

/**
 * Implements hook_locale_default_languages().
 */
function abeancos_locale_default_languages() {
  $languages = array();

  // Exported language: gl.
  $languages['gl'] = array(
    'language' => 'gl',
    'name' => 'Galician',
    'native' => 'Galego',
    'direction' => 0,
    'enabled' => 1,
    'plurals' => 2,
    'formula' => '($n!=1)',
    'domain' => '',
    'prefix' => 'gl',
    'weight' => 0,
  );
  return $languages;
}
