<?php
/**
 * @file
 * abeancos.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function abeancos_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: main-menu.
  $menus['main-menu'] = array(
    'menu_name' => 'main-menu',
    'title' => 'Menú principal',
    'description' => 'O menú <em>Principal</em> empregase en moitos sitios web para amosar as seccións principais do sitio, xeralmente nunha barra de navegación.',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Menú principal');
  t('O menú <em>Principal</em> empregase en moitos sitios web para amosar as seccións principais do sitio, xeralmente nunha barra de navegación.');

  return $menus;
}
