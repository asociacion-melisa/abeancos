<?php
/**
 * @file
 * abeancos.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function abeancos_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "openlayers" && $api == "openlayers_maps") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function abeancos_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function abeancos_node_info() {
  $items = array(
    'elemento' => array(
      'name' => t('Elemento'),
      'base' => 'node_content',
      'description' => t('Elementos do catálogo de Abeancos'),
      'has_title' => '1',
      'title_label' => t('Nome'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
