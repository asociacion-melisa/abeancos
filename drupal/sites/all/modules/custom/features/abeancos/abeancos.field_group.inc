<?php
/**
 * @file
 * abeancos.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function abeancos_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_clasificacion|node|elemento|form';
  $field_group->group_name = 'group_clasificacion';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'elemento';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Clasificación',
    'weight' => '1',
    'children' => array(
      0 => 'field_acceso',
      1 => 'field_tipo',
      2 => 'field_estado_de_conservacion',
    ),
    'format_type' => 'htabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => 'group-clasificacion field-group-htabs',
        'id' => '',
      ),
    ),
  );
  $field_groups['group_clasificacion|node|elemento|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_datos|node|elemento|form';
  $field_group->group_name = 'group_datos';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'elemento';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Datos',
    'weight' => '0',
    'children' => array(
      0 => 'body',
      1 => 'field_concello',
      2 => 'field_cronoloxia',
      3 => 'title',
    ),
    'format_type' => 'htabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => 'group-datos field-group-htabs',
        'id' => '',
      ),
    ),
  );
  $field_groups['group_datos|node|elemento|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_multimedia|node|elemento|form';
  $field_group->group_name = 'group_multimedia';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'elemento';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Multimedia',
    'weight' => '2',
    'children' => array(
      0 => 'field_audio',
      1 => 'field_imaxe',
      2 => 'field_localizacion',
    ),
    'format_type' => 'htabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => 'group-multimedia field-group-htabs',
        'id' => '',
      ),
    ),
  );
  $field_groups['group_multimedia|node|elemento|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Clasificación');
  t('Datos');
  t('Multimedia');

  return $field_groups;
}
