<?php
/**
 * @file
 * abeancos.openlayers_maps.inc
 */

/**
 * Implements hook_openlayers_maps().
 */
function abeancos_openlayers_maps() {
  $export = array();

  $openlayers_maps = new stdClass();
  $openlayers_maps->disabled = FALSE; /* Edit this to true to make a default openlayers_maps disabled initially */
  $openlayers_maps->api_version = 1;
  $openlayers_maps->name = 'elementos_abeanco_ol_map';
  $openlayers_maps->title = 'elementos_abeancos_ol_map';
  $openlayers_maps->description = '';
  $openlayers_maps->data = array(
    'width' => '800px',
    'height' => '400px',
    'image_path' => 'sites/all/modules/openlayers/themes/default_dark/img/',
    'css_path' => 'sites/all/modules/openlayers/themes/default_dark/style.css',
    'proxy_host' => '',
    'hide_empty_map' => 0,
    'center' => array(
      'initial' => array(
        'centerpoint' => '-7.9595948329186, 42.90413656762186',
        'zoom' => '9',
      ),
      'restrict' => array(
        'restrictextent' => 0,
        'restrictedExtent' => '',
      ),
    ),
    'behaviors' => array(
      'openlayers_behavior_cluster' => array(
        'clusterlayer' => array(
          'mapa_abeancos_openlayers_1' => 'mapa_abeancos_openlayers_1',
          'geofield_formatter' => 0,
        ),
        'distance' => '20',
        'threshold' => '',
        'display_cluster_numbers' => 1,
        'middle_lower_bound' => '15',
        'middle_upper_bound' => '50',
        'low_color' => 'rgb(141, 203, 61)',
        'low_stroke_color' => 'rgb(141, 203, 61)',
        'low_opacity' => '0.8',
        'low_point_radius' => '10',
        'low_label_outline' => '1',
        'middle_color' => 'rgb(49, 190, 145)',
        'middle_stroke_color' => 'rgb(49, 190, 145)',
        'middle_opacity' => '0.8',
        'middle_point_radius' => '16',
        'middle_label_outline' => '1',
        'high_color' => 'rgb(35, 59, 177)',
        'high_stroke_color' => 'rgb(35, 59, 177)',
        'high_opacity' => '0.8',
        'high_point_radius' => '22',
        'high_label_outline' => '1',
        'label_low_color' => '#000000',
        'label_low_opacity' => '0.8',
        'label_middle_color' => '#000000',
        'label_middle_opacity' => '0.8',
        'label_high_color' => '#000000',
        'label_high_opacity' => '0.8',
      ),
      'openlayers_behavior_fullscreen' => array(
        'activated' => 1,
      ),
      'openlayers_behavior_geofield' => array(),
      'openlayers_behavior_hover' => array(
        'layers' => array(
          'mapa_abeancos_openlayers_1' => 'mapa_abeancos_openlayers_1',
          'geofield_formatter' => 0,
        ),
      ),
      'openlayers_behavior_keyboarddefaults' => array(),
      'openlayers_behavior_attribution' => array(
        'separator' => '',
      ),
      'openlayers_behavior_layerswitcher' => array(
        'ascending' => 1,
        'sortBaseLayer' => '0',
        'roundedCorner' => 1,
        'roundedCornerColor' => '#222222',
        'maximizeDefault' => 0,
        'div' => '',
      ),
      'openlayers_behavior_navigation' => array(
        'zoomWheelEnabled' => 1,
        'zoomBoxEnabled' => 1,
        'documentDrag' => 0,
      ),
      'openlayers_behavior_panzoombar' => array(
        'zoomWorldIcon' => 0,
        'panIcons' => 1,
      ),
      'openlayers_behavior_popup' => array(
        'layers' => array(
          'mapa_abeancos_openlayers_1' => 'mapa_abeancos_openlayers_1',
          'geofield_formatter' => 0,
        ),
        'popupAtPosition' => 'mouse',
        'panMapIfOutOfView' => 0,
        'keepInMap' => 1,
      ),
    ),
    'default_layer' => 'mapquest_osm',
    'layers' => array(
      'mapquest_osm' => 'mapquest_osm',
      'mapquest_openaerial' => 'mapquest_openaerial',
      'osm_mapnik' => 'osm_mapnik',
      'osm_cycle' => 'osm_cycle',
      'geofield_formatter' => 'geofield_formatter',
      'mapa_abeancos_openlayers_1' => 'mapa_abeancos_openlayers_1',
    ),
    'layer_weight' => array(
      'ip_geoloc_marker_layer3' => '0',
      'geofield_formatter' => '0',
      'mapa_abeancos_openlayers_1' => '0',
      'ip_geoloc_marker_layer2' => '0',
      'ip_geoloc_marker_layer1' => '0',
      'openlayers_geojson_picture_this' => '0',
      'ip_geoloc_visitor_marker_layer' => '0',
      'openlayers_kml_example' => '0',
    ),
    'layer_styles' => array(
      'openlayers_kml_example' => '0',
      'openlayers_geojson_picture_this' => '0',
      'ip_geoloc_visitor_marker_layer' => '0',
      'ip_geoloc_marker_layer1' => '0',
      'ip_geoloc_marker_layer2' => '0',
      'ip_geoloc_marker_layer3' => '0',
      'geofield_formatter' => '0',
      'mapa_abeancos_openlayers_1' => '0',
    ),
    'layer_styles_select' => array(
      'openlayers_kml_example' => '0',
      'openlayers_geojson_picture_this' => '0',
      'ip_geoloc_visitor_marker_layer' => '0',
      'ip_geoloc_marker_layer1' => '0',
      'ip_geoloc_marker_layer2' => '0',
      'ip_geoloc_marker_layer3' => '0',
      'geofield_formatter' => '0',
      'mapa_abeancos_openlayers_1' => '0',
    ),
    'layer_styles_temporary' => array(
      'openlayers_kml_example' => '0',
      'openlayers_geojson_picture_this' => '0',
      'ip_geoloc_visitor_marker_layer' => '0',
      'ip_geoloc_marker_layer1' => '0',
      'ip_geoloc_marker_layer2' => '0',
      'ip_geoloc_marker_layer3' => '0',
      'geofield_formatter' => '0',
      'mapa_abeancos_openlayers_1' => '0',
    ),
    'layer_activated' => array(
      'geofield_formatter' => 'geofield_formatter',
      'mapa_abeancos_openlayers_1' => 'mapa_abeancos_openlayers_1',
      'openlayers_kml_example' => 0,
      'openlayers_geojson_picture_this' => 0,
      'ip_geoloc_visitor_marker_layer' => 0,
      'ip_geoloc_marker_layer1' => 0,
      'ip_geoloc_marker_layer2' => 0,
      'ip_geoloc_marker_layer3' => 0,
    ),
    'layer_switcher' => array(
      'geofield_formatter' => 0,
      'openlayers_kml_example' => 0,
      'openlayers_geojson_picture_this' => 0,
      'mapa_abeancos_openlayers_1' => 0,
      'ip_geoloc_visitor_marker_layer' => 0,
      'ip_geoloc_marker_layer1' => 0,
      'ip_geoloc_marker_layer2' => 0,
      'ip_geoloc_marker_layer3' => 0,
    ),
    'projection' => 'EPSG:3857',
    'displayProjection' => 'EPSG:4326',
    'styles' => array(
      'default' => 'default_marker_green',
      'select' => 'default_marker_green',
      'temporary' => 'default_marker_green',
    ),
  );
  $export['elementos_abeanco_ol_map'] = $openlayers_maps;

  return $export;
}
