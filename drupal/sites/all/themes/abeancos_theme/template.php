<?php
/**
 * @file
 * The primary PHP file for this theme.
 */

function abeancos_multimedia($nid){

        $elemento = node_load($nid);

        $ruta = field_get_items('node', $elemento, 'field_ruta');
        $lenda = field_get_items('node', $elemento, 'field_audio');
        $tresd = field_get_items('node', $elemento, 'field_modelo_3d_2');
        $video = field_get_items('node', $elemento, 'field_video');

        if($ruta){
                $result[$nid]['ruta']['icon'] = 'glyphicon glyphicon-map-marker';
                $result[$nid]['ruta']['alt'] = 'Contén indicacións';

        }

        if($lenda){
                $result[$nid]['lenda']['icon'] = 'glyphicon glyphicon-volume-up';
                $result[$nid]['lenda']['alt'] = 'Contén son';
        }

        if($tresd){
                $result[$nid]['tresd']['icon'] = 'glyphicon glyphicon-fullscreen';
                $result[$nid]['tresd']['alt'] = 'Contén 3D';
        }

        if($video){
                $result[$nid]['video']['icon'] = 'glyphicon glyphicon-facetime-video';
                $result[$nid]['video']['alt'] = 'Contén vídeo';
        }

        return $result;

}
