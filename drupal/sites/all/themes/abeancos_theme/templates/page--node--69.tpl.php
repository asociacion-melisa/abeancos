<div class="wide">
<div class="row">
    <div class="col-md-12">
	<!-- desktop -->
     <p class="text-center hidden-xs" style="margin: 130px 0px 180px 0px; font-size: 5em; color: #ffffff; font-family: 'Gallaecia'; text-shadow: 0px 2px 3px #555;">Abeancos.gal</p>
	<!-- mobile -->
     <p class="text-center visible-xs" style="margin: 130px 0px 180px 0px; font-size: 2.5em; color: #ffffff; font-family: 'Gallaecia'; text-shadow: 0px 2px 3px #555;">Abeancos.gal</p>

      </div>
    
    <div class="col-md-12 text-center">
        <a href="/catalogo_elementos"><button class="btn btn-default btn-lg active">Consultar catálogo</button></a>
        <a href="/mapa"><button class="btn btn-default btn-lg active">Ver mapa</button></a>
    </div>

</div> <!-- row -->
</div>


    <div class="container marketing" style="margin-top: 20px;">

      <!-- Tres columnas -->
      <div class="row">
        <div class="col-lg-4">
          <h2>O proxecto</h2>
        <p><b>abeancos.gal</b> é un catálogo dixital aberto de elementos patrimoniais presentes na área da Comarca Terra de Melide. Todos eles, documentados, clasificados e xeolocalizados con mapas libres.</p>  
	<p><a href="/presentacion" class="btn btn-default" href="#" role="button">Coñece o proxecto  &raquo;</a></p>
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4">
          <h2>Catálogo aberto</h2>
        <p>Ao tratarse dun <b>catálogo dixital aberto</b>, o acceso aos datos non está restrinxido unicamente á súa visualización neste sitio web. O acceso aos datos en distintos formatos é libre, e gratuíto. </p>  
	<p><a href="/datos_abertos" class="btn btn-default" href="#" role="button">Máis información &raquo;</a></p>
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4">
          <h2>Software libre</h2>
        <p>abeancos.gal <b>é un proxecto libre</b>. A súa base tecnolóxica, e os seus compoñentes: mapas, imaxes, documentos, xestor de contidos, etc. están publicados baixo licenzas libres.</p>
	<p><a class="btn btn-default" href="software-libre" role="button">Saber máis &raquo;</a></p>
        </div><!-- /.col-lg-4 -->
      </div><!-- /.row -->


    <hr class="featurette-divider">


      <!-- /END THE FEATURETTES -->


      <!-- FOOTER -->
      <footer style="margin-top: 70px">
        <p class="text-center"><a href="http://webmelisa.es">Asociación Melisa</a> &middot; <a href="termos">Termos e condicións de uso</a></p>
	  <p class="text-center">Feito en Melide, Ames e Mugardos con <span class="glyphicon glyphicon-heart-empty" aria-hidden="true"></span> paixón <span class="glyphicon glyphicon-heart-empty" aria-hidden="true"></span>  e software libre.</p>    
	<div style="text-align:center">
	<img src="sites/default/files/Logos-xunta.png" align="center">
	</div>
 </footer>


