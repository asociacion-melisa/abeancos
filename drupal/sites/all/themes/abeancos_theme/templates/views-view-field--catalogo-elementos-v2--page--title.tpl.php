<?php

/**
 *  * @file
 *   * This template is used to print a single field in a view.
 *    *
 *     * It is not actually used in default Views, as this is registered as a theme
 *      * function which has better performance. For single overrides, the template is
 *       * perfectly okay.
 *        *
 *         * Variables available:
 *          * - $view: The view object
 *           * - $field: The field handler object that can process the input
 *            * - $row: The raw SQL result that can be used
 *             * - $output: The processed output that will normally be used.
 *              *
 *               * When fetching output from the $row, this construct should be used:
 *                * $data = $row->{$field->field_alias}
 *                 *
 *                  * The above will guarantee that you'll always get the correct data,
 *                   * regardless of any changes in the aliasing that might happen if
 *                    * the view is modified.
 *                     */


$codigo = abeancos_multimedia($row->nid);

?>

<?php print $output; ?>
<p>
<?php
	if(isset($codigo[$row->nid]['ruta'])){
		print '<span class="'.$codigo[$row->nid]['ruta']['icon'] .' iconos-multimedia" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" data-title="'. $codigo[$row->nid]['ruta']['alt'] .'"></span>';
	}

	if(isset($codigo[$row->nid]['lenda'])){
               print '<span class="'.$codigo[$row->nid]['lenda']['icon'] .' iconos-multimedia" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" data-title="'. $codigo[$row->nid]['lenda']['alt'] .'"></span>';
	}

	if(isset($codigo[$row->nid]['tresd'])){
	      print '<span class="'.$codigo[$row->nid]['tresd']['icon'] .' iconos-multimedia" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" data-title="'. $codigo[$row->nid]['tresd']['alt'] .'"></span>';
        }

        if(isset($codigo[$row->nid]['video'])){
		print '<span class="'.$codigo[$row->nid]['video']['icon'] .' iconos-multimedia" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" data-title="'. $codigo[$row->nid]['video']['alt'] .'"></span>';
			              }

?>
</p>
